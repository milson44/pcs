@extends('layouts.app')

@section('content')
<style>
body{
    box-sizing: border-box;
}
</style>
 <div class="container-fluid" style="max-width:1200px;margin-top:3%">
   <div class="row">
     <div class="col-md-4">
       <div class="color" style="margin-right:20px;">

         <p class="color font2" style="margin-bottom: 7%;">Страна</p>
         <span class="dropdown">
           <a href="#" class="dropdown-toggle font" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
             Российская Федерация <i style="padding-left:49px;"class="fa fa-angle-down fa-lg"></i>
           </a>
           <ul class="dropdown-menu">
             <li><a href="#">Турция</a></li>
             <li><a href="#">Германия</a></li>
             <li><a href="#">Польша</a></li>
             <li><a href="#">Чехия</a></li>
             <li><a href="#">Бельгия</a></li>
           </ul>
         </span>
         <hr style="border-top:2px solid #086972">
         <style>
         .checkbox1 label{
           padding-left: 0px;
         }
         </style>
         <p class="color font2" style="margin-bottom: 7%;"> Тип</p>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check1" name="check" checked><label for="check1">Физическое лицо </label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check2" name="check"><label for="check2">Юридическое лицо</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check3" name="check"><label for="check3">ИП</label>
         </div>
         <hr style="border-top:2px solid #086972">

         <p class="color font2" style="margin-bottom: 7%;"> Владеет</p>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check4" name="check" checked><label for="check4">Патент</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check5" name="check"><label for="check5">Авторское право</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check6" name="check" ><label for="check6">Аудио произведение</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check7" name="check" ><label for="check7">Видео произведение</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check8" name="check" ><label for="check8">Текстовое произведение</label>
         </div>
         <hr style="border-top:2px solid #086972">
       </div>
     </div>
     <div class=" col-md-8">
       <div class="search">
         <input type="search" name="q" placeholder="Поиск по сайту Введите название услуги"/>
         <i type="submit" class="fa fa-search fa-2x isearch" aria-hidden="true" style="margin-top:-3px"></i>
       </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

           <!--<ul class="nav navbar-nav navbar-left font">
            <li style="float:left;"><a>Найдено 4</a></li>
          </ul>-->
           <ul class="nav navbar-nav navbar-right font">
           <li><a>Сортировать по</a></li>
           <li class="dropdown">
             <a href="#" class="dropdown-toggle color" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
               Стране <i class="fa fa-angle-down fa-lg"></i>
             </a>
             <ul class="dropdown-menu">
               <li><a href="#">Алфавиту</a></li>
               <li><a href="#">Геопозиции</a></li>
             </ul>
           </li>
         </ul>
       </div>
       <div class="" >

         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Иванов Иван Иванович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;">  Сидоров Федор Васильевич</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Истоцкий Валерий Эдуардович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Иванов Иван Иванович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;">  Сидоров Федор Васильевич</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Истоцкий Валерий Эдуардович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Иванов Иван Иванович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;">  Сидоров Федор Васильевич</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Истоцкий Валерий Эдуардович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>
         <div class="green_block row" style="height:130px">
           <div class="col-md-3 one" style="padding:20px">
             <div class="square"></div>
           </div>
           <div class="col-md-9 two" style="padding:20px">
             <p class="color" style="margin-bottom:0px; font-size:22px;"> Иванов Иван Иванович</p>
             <p class="font" style="color:#199384;font-size:18px; font-weight:600">Физическое лицо</p>
             <p class="font blisspro">Россия</p>

           </div>
         </div>

         </div>

       </div>
     </div>
     <div class="col-md-12" style="text-align:center">
       <div class="pagination">
          <a href="#">&laquo;</a>
          <a href="#">1</a>
          <a class="active" href="#">2</a>
          <a href="#">3</a>
          <a href="#">4</a>
          <a href="#">5</a>
          <a href="#">6</a>
          <a href="#">&raquo;</a>
      </div>
   </div>
 </div>
@endsection
