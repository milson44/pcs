@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div>
   <ul class="nav navbar-nav navbar-left">
     <li class="blisspro" style="margin-right:40px;"><a class="color" href="#">Каталог услуг</a></li>
     <li class="blisspro"><a class="color" href="#">Помощь и поддержка</a></li>
   </ul>
   <ul class="nav navbar-nav navbar-right">
     <li class="blisspro"><a class="color" href="{{ url('/user') }}"><img src="{{asset('img/key.png')}}" style="width:25px; margin-right:10px; margin-top:-3px;"/>Личный кабинет</a></li>
   </ul>
 </div>
   <div class="row">
     <div class=" col-md-12 ">
       <div class="search">
         <input type="search" name="q" placeholder="Поиск по сайту Введите название услуги"/>
         <i type="submit" class="fa fa-search fa-2x isearch" aria-hidden="true" style="margin-top:-3px"></i>
       </div>
     </div>
   </div>
 </div>
 <div class="container-fluid">
   <div class="row bigdown">
     <div class="col-md-9">
       <h2 class="color center blisspro" style="font-size:30px;">Цели проекта<h2>
         <div class="font">
           <p><b> Платформа Цифровой Собственности</b> позволяет зарегистрировать цифровую собственность
             максимально простым способом: без поездок организации, очередей, заполнения огромного количества
             документов и прочих изнурительных процедур.</p>

           <p><b>Просто:</b> Зарегистрировать, купить, продать цифровую собственность можно прямо на сайте.
             Регистрация собственности занимает несколько минут.</p>

           <p><b>Безопасно: </b>Децентрализация с помощью блокчейн позволяет сделать платформу безопасной.
             Все сделки выполняются под контролем смартконтракта.</p>
           </div>
     </div>
     <div class="col-md-3">
       <div class="login">
         <h2 class="color center blisspro" style="font-size:30px;">Войти<h2>
          <p class="font down center" style="font-size:18px;">Войдите или зарегистрируйтесь,
              чтобы получить полный доступ
              ко всем услугам
          </p>
          <div class="down" style="text-align:center">
            <a href="{{ url('/login') }}" class="btn" style="background-color:#086972;">Войти</a>
          </div>
          <div class="down" style="text-align:center">
            <a href="{{ url('/register') }}" class="btn" style="background-color: #199384;">Зарегистрироваться</a>
          </div>
       </div>
     </div>
   </div>
   <div class="row bigdown">
     <div class="col-md-12">

       <h2 class="color center bigdown blisspro" style=";font-size:30px;">Наши услуги<h2>
      </div>
      <div class="row bigdown">
        <div class="col-md-4 center service">
          <img src="{{asset('img/reg.png')}}"  aria-hidden="true" class="icon"/>
          <h2 class="color ">Зарегистрировать</h2>
          <div class="font">
            <p>Зарегистрируйте цифровую собственность онлайн, закрепив за собой права обладания</p>
          </div>
        </div>
        <div class="col-md-4 center service">
          <img src="{{asset('img/bag.png')}}"  aria-hidden="true" class="icon"/>
          <h2 class="color">Купить</h2>
          <div class="font">
            <p>Приобретите права использования или выкупите авторские права на цифровую собственность</p>
          </div>
        </div>
        <div class="col-md-4 center service">
          <img src="{{asset('img/sale.png')}}"  aria-hidden="true" class="icon"/>
          <h2 class="color">Оценить</h2>
          <div class="font">
            <p>Оцените стоимость или ознакомьтесь с представленными товарами</p>
          </div>
        </div>
      </div>
      <div class="down" style="text-align:center">
        <a href="#" class="btn" style="background-color:#199384; width:auto; padding-right:5px">Все услуги
        <img src="{{asset('img/next.png')}}" style="width:20px;margin-top:-2px"/></a>
      </div>
    </div>
    <div class="row popular bigdown">
      <div class="col-md-12">
        <h2 class="color center blisspro" style=";font-size:30px;">Популярные вопросы<h2>
        <p class="font center bigdown blisspro">При работе с платформой</p>
       </div>

       <div class="row bigdown color">
         <div class="col-md-6" style="padding:0px 40px;">
           <p class="font down"><a href="#">Как зарегистрировать цифровую собственность?</a></p>
         </div>
         <div class="col-md-6" style="padding:0px 10px;">
           <p class="font down"><a href="#">Что такое смартконтракт и чем он лучше стандартных алгоритмов?</a></p>
         </div>
         <div class="col-md-6" style="padding:0px 40px;">
           <p class="font down"><a href="#">Какие объекты можно зарегистрировать в ПЦС?</a></p>
         </div>
         <div class="col-md-6" style="padding:0px 10px;">
           <p class="font down"><a href="#">Как получить права использования на чужую собственность?</a></p>
         </div>
           <div class="col-md-6" style="padding:0px 40px;">
           <p class="font down"><a href="#">Что такое уникальный токен цифровой собственности?</a></p>
         </div>
         <div class="col-md-6" style="padding:0px 10px;">
           <p class="font down"><a href="#">Какие услуги на площадке предоставляют гаранты?</a></p>
         </div>
           <div class="col-md-6" style="padding:0px 40px;">
           <p class="font down"><a href="#">Как найти определенный товар на бирже?</a></p>
         </div>
         <div class="col-md-6" style="padding:0px 10px;">
           <p class="font down"><a href="#">Как найти определенный товар на бирже?</a></p>
         </div>
           <div class="col-md-6" style="padding:0px 40px;">
           <p class="font down"><a href="#">Что такое уникальный токен цифровой собственности?</a></p>
         </div>
         <div class="col-md-6" style="padding:0px 10px;">
           <p class="font down"><a href="#">Что такое уникальный токен цифровой собственности?</a></p>
         </div>
         </div>

         <div class="" style="text-align:center">
           <a href="#" class="btn" style="background-color:#199384; width:auto;padding-right:5px">Все вопросы
             <img src="{{asset('img/next.png')}}" style="width:20px; margin-top:-2px;"/></a>
         </div>
       </div>






     <div class="row">
       <div class="col-md-3">
         <h2 class="color center blisspro" style=";font-size:30px;">Новости платформы<h2>
       </div>
       <div class="col-md-9" style="margin-top:27px;">
         <div class="news">
           <p class="color blisspro">01.01.2017</p>
           <p class="color blisspro" style="font-size:23px;">Мы подготовили все материалы и готовы выйти на PreICO</p>
           <p class="font">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id tortor sed est tempus ullamcorper. Mauris tortor sem, sollicitudin id pellentesque id, rhoncus ut quam.</p>
         </div>
         <div class="news">
           <p class="color blisspro">01.01.2017</p>
           <p class="color blisspro" style="font-size:23px;">На ПЦС проведена первая тестовая сделка</p>
           <p class="font">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id tortor sed est tempus ullamcorper. Mauris tortor sem, sollicitudin id pellentesque id, rhoncus ut quam.</p>
         </div>
         <div class="news">
           <p class="color blisspro">01.01.2017</p>
           <p class="color blisspro" style="font-size:23px;">Мы собрали первую тестовую версию сайта</p>
           <p class="font">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id tortor sed est tempus ullamcorper. Mauris tortor sem, sollicitudin id pellentesque id, rhoncus ut quam.</p>
         </div>
       </div>
     </div>

</div>
@endsection
