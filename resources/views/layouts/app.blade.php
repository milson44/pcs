<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Платформа Цифровой Собственности</title>

        <!-- Fonts -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">

        <!-- Styles -->

    </head>
    <body>

      <nav class="navbar navbar-default">
        <div class="container-fluid" style="margin-bottom:0px;">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="padding:10px; padding-left:15px;" href="{{ url('/') }}">
              <b class="brand DaySansBlack" style="font-size:25px;">ПЦС</b>
              <br><span class="blisspro" style="font-size:17px;">Платформа Цифровой Собственности</span></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            <ul class="nav navbar-nav navbar-right">
              <li style="margin-right:40px;">
                <a href="#" class="blisspro"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right:15px;"></i> Москва</a></li>
              <li class="dropdown blisspro">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <img src="{{asset('img/russia.png')}}" style="width:30px;margin-right:15px;"/>Русский <img src="{{asset('img/down.png')}}" style="width:20px;margin-right:10px;"/>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#">Английский</a></li>
                  <li><a href="#">Русский</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>


      <div id="app" class="container-fluid">
        @yield('content')
      </div>

      <footer>
        <div class="row ">
          <div class="col-md-6">
            <h2 class="blisspro" ><b class="DaySansBlack">П</b>латформа <b class="DaySansBlack">Ц</b>ифровой <b class="DaySansBlack">С</b>обственности</h2>
          </div>
          <div class="col-md-6">
            <div class="social_icons">
              <ul>
                <li><a href=""><img src="{{asset('img/vk.png')}}" /></a></li>
                <li><a href=""><img src="{{asset('img/facebook.png')}}" /></a></li>
                <li><a href=""><img src="{{asset('img/twitter.png')}}"/></a></li>
                <li><a href=""><img src="{{asset('img/classmate.png')}}" /></a></li>
              </ul>
            </div>

          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="color_white blisspro">Помощь и поддержка</p>
            <p class="color_white2"><a href="#">Популярные вопросы</a></p>
            <p class="color_white2"><a href="#">Хадать вопрос</a></p>
            <p class="color_white2"><a href="#">Связаться с нами</a></p>
          </div>
          <div class="col-md-4">
            <p class="color_white blisspro">Информация о платформе</p>
            <p class="color_white2"><a href="#">Наши услуги</a></p>
            <p class="color_white2"><a href="#">Используемые технологии</a></p>
            <p class="color_white2"><a href="#">Защита персональных данных</a></p>
            <p class="color_white2"><a href="#">Пользовательское соглашение</a></p>
            <p class="color_white2"><a href="#">Политика конфиденциальности</a></p>
          </div>
          <div class="col-md-4">
            <p class="color_white blisspro">Партнеры и вакансии</p>
            <p class="color_white2"><a href="#">Вакансии</a></p>
            <p class="color_white2"><a href="#">Партнеры</a></p>
          </div>
        </div>
      </footer>


      <script src="{{ asset('js/jquery.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    </body>
</html>
