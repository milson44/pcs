@extends('layouts.app')

@section('content')
<div class="bigdown" style="text-align:-webkit-center;">
  <h1 class="color blisspro" style="font-size:30px;">Регистрация интеллектуальной собственности</h1>
  <hr style="border-top:2px solid #c8fbd4; margin:2% 0;">
  <div class="row bigdown">
    <div class="col-md-offset-3 col-md-6">
      <div class="col-md-12" style="margin-bottom:3%">
        <p class="color blisspro" style="font-size:28px;">Информация о владельце</p>

      </div>
      <div class="row">
        <div class="col-md-4">
          <p class="color" style="float:right;font-weight:500">Тип субъекта</p>
        </div>
        <div class="col-md-8">
          <p class="color blisspro" style="float:left;font-weight:600">Физическое лицо</p>
        </div>
        <div class="col-md-4">
          <p class="color" style="float:right;font-weight:500">Фамилия</p>
        </div>
        <div class="col-md-8">
          <p class="color blisspro" style="float:left;font-weight:600">Иванов</p>
        </div>
        <div class="col-md-4">
          <p class="color" style="float:right;font-weight:500">Имя</p>
        </div>
        <div class="col-md-8">
          <p class="color blisspro" style="float:left;font-weight:600">Иван</p>
        </div>
        <div class="col-md-4">
          <p class="color" style="float:right;font-weight:500">Отчество</p>
        </div>
        <div class="col-md-8">
          <p class="color blisspro" style="float:left;font-weight:600">Иванов</p>
        </div>
        <div class="col-md-4">
          <p class="color" style="float:right;font-weight:500">Телефон</p>
        </div>
        <div class="col-md-8">
          <p class="color blisspro" style="float:left;font-weight:600">8-(999)-999-9999</p>
        </div>


        </div>
      </div>
    </div>
    <div class="row">

          <div class="col-md-12" style="margin-bottom:3%">
            <p class="color blisspro" style="font-size:28px;">Информация об интеллектуальной собственности</p>
          </div>
          <div class="col-md-offset-3 col-md-6">
            <form class="form-horizontal" method="POST" action="">
            <div class="form-group">
              <label for="class" class="col-md-4 control-label color up" style="font-weight:500">Вид собственности</label>
              <div class="col-md-8">
                    <select class="form-control blisspro" name="class" style="font-size:18px; padding:3px 7px;">
                        <option>Вид собственности</option>
                        <option>Вид собственности 2</option>
                        <option>Вид собственности 3</option>
                        <option>Вид собственности 4</option>
                        <option>Вид собственности 5</option>
                    </select>

                  </div>
                </div>
                <div class="form-group">
                  <label for="object" class="col-md-4 control-label color up" style="font-weight:500">Наименование</label>
                    <div class="col-md-8">
                      <input id="object" type="text" class="form-control" name="object" required autofocus>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-md-4 control-label color up" style="font-weight:500">Краткое описание</label>
                      <div class="col-md-8">
                        <textarea rows="10" cols="45" type="text" class="form-control" name="description" required autofocus></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-offset-4 col-md-8">
                        <label class="green">
                            <input id="file" type="file"  name="file" required style="display:none">
                                <span>Перетащите файл или кликните, чтобы выбрать</span><br />
                          </label>

                        </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-offset-4 col-md-8">
                        <label class="green">
                            <input id="file" type="file"  name="file" required style="display:none">
                                <span>Перетащите копию документа, подтверждающего
                                право владения интеллектуальной собственностью</span><br />
                          </label>

                        </div>
                    </div>
                    <div class="form-group">
                      <label for="tag" class="col-md-4 control-label color up" style="font-weight:500">Добавьте теги</label>
                        <div class="col-md-8">
                          <input id="tag" type="text" class="form-control" name="tag" required autofocus>
                        </div>
                      </div>
                      <div class="form-group" >
                        <div class="col-md-4"></div>
                          <div class=" col-md-8 ">
                              <button type="submit" class="btn btn-primary" style="background-color:#086972; font-size:19px;">
                                  Зарегистрировать собственность
                              </button>
                          </div>
                      </div>
                      </form>

  </div>
</div>
</div>
@endsection
