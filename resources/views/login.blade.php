<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Платформа Цифровой Собственности</title>


        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">

        <!-- Styles -->

    </head>
    <body>
      <style>
      html, body{
        background-color:#f2f2f2
      }
      .login{
        background-color:#fff;
        border: 2px solid #6bb8ae;
        border-radius: 10px;
      }
      .title{
        margin-top: 3%;
      }
      .color{
        font-size: 25px;
      }
       .social_icons ul li a {
         color:#fff;
       }
       .form-control{
         border: 2px solid #6bb8ae;
       }
      </style>
      <div class="bigdown" style="text-align:-webkit-center;">

          <div class="title bigdown blisspro">
            <a class="brand" href="/">
            <p><b class="DaySansBlack">П</b>латформа</p>
            <p><b class="DaySansBlack">Ц</b>ифровой</p>
            <p><b class="DaySansBlack">С</b>обственности</p>
          </a>
          </div>
          <div class="row" style="width:100%;">
            <p class="color blisspro">Получите полный доступ к услугам</p>

            <div class="login" style="margin:0 38%">
              <form class="form-horizontal" method="POST" action="">
                  <div class="form-group">
                      <div class="col-md-12">
                        <label for="email" class="col-md-12 control-label" style="text-align:center; margin-bottom:2%;">Мобильный телефон или почта</label>
                          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="+7 (999) 999 99 99">
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <input id="password" type="password" class="form-control" name="password" required placeholder="Пароль">
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                        <button type="submit" class="btn btn-primary" style="background-color:#086972; margin-bottom:15px;">
                            Войти
                        </button>
                          <div class="checkbox1">
                            <style>
                            .checkbox1 label::before{
                              left:0px;
                            }
                            .checkbox1 label::after{
                              left: 4px;
                            }
                            .checkbox1 label {
                              padding-left: 30px;
                            }
                            </style>

                                  <input type="checkbox" id="check" name="check"/>
                              <label style="float:left;"for="check">Чужой компьютер</label>
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <button type="submit" class="btn btn-primary" style="background-color:#199384">
                              Зарегистрироваться
                          </button>

                          <a class="btn btn-link bigbtn font" href="">
                            Восстановить пароль
                          </a>
                      </div>
                  </div>
              </form>
          </div>
        </div>
      </div>
<div class="footer">
  <div class=" container-fluid" style="border-top:2px solid #6bb8ae">
  <div class="row">
    <div class="col-md-6">
      <a class="brand" href="/">
      <h2 class="blisspro" style="color: #1b435d;">
        <b class="DaySansBlack" style="color: #ff895d;">П</b>латформа
        <b class="DaySansBlack" style="color: #ff895d;">Ц</b>ифровой
        <b class="DaySansBlack" style="color: #ff895d;">С</b>обственности
      </h2>
      </a>
    </div>
    <div class="col-md-6">
      <div class="social_icons">
        <ul>
          <li><a href=""><img src="{{asset('img/vk_green.png')}}" /></a></li>
          <li><a href=""><img src="{{asset('img/facebook_green.png')}}" /></a></li>
          <li><a href=""><img src="{{asset('img/twitter_green.png')}}"/></a></li>
          <li><a href=""><img src="{{asset('img/classmate_green.png')}}" /></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
