@extends('layouts.app')

@section('content')
<div class=" col-md-offset-2 col-md-8">
  <div class="user_info row">
    <div class="col-md-4">
      <div class="square" style="max-height:200px; margin-top:25px; margin-right:20px;"></div>
    </div>
    <div class="col-md-8"  style="padding-left:0px; margin-bottom:3%">
      <h1 class="color blisspro" style="font-size: 36px; margin-bottom:7%">Иванов Иван Иванович</h1>

      <div class="col-md-4" style="padding-left:0px">
        <p class="font font2">Страна:</p>
        <p class="font font2">Адрес:</p>
    </div>
    <div class="col-md-8">
      <p class="font blisspro">Российская Федерация</p>
      <p class="font blisspro">г.Казань, ул.Калинина, д.25, оф.5</p>
    </div>
  </div>
</div>
  <div class="user_info row" style="border-bottom:2px solid #086972">

    <div class="col-md-offset-4 col-md-8" style="border-top:2px solid #086972; padding-left:0px; margin-bottom:3%">



        <h2 class="color" style="color:#086972;font-size:25px; margin-bottom:4%">Контакты</h2>

      <div class="col-md-4" style="padding-left:0px">
        <p class="font font2">Телефон:</p>
        <p class="font font2">Email:</p>
    </div>
    <div class="col-md-5">
      <p class="font blisspro" id="phone_number">***********</p>
      <p class="font blisspro" id="mail">**************************</p>
    </div>
    <div class="col-md-3">
      <p class="font font2" id="switcher1" style="cursor:pointer;float:right">Показать</p>
      <p class="font font2" id="switcher2" style="cursor:pointer;float:right">Показать</p>
    </div>
    </div>
  </div>
  <div class="" style="">
    <h2 class="color" style="color:#086972;;font-size:25px;margin-bottom:4%">Собственность</h2>
    <div class="green_block row">
      <div class="col-md-2 one" style="padding:30px;padding-right: 0">
        <div class="square"></div>
      </div>
      <div class="col-md-7 two" style="padding:30px;">
        <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
        <p class="font" style="color:#199384;font-size:16px; font-weight:500">Россия <span style="margin-left:10px;">01.05.2018</span></p>
        <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

      </div>
      <div class="col-md-3 three" style="padding:30px;">
        <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
        <p class="color" style="color:#fff;">5000 Р</p>
        <button class="btn bigbtn call">Связаться</button>
      </div>

    </div>
    <div class="green_block row">
      <div class="col-md-2 one" style="padding:30px;padding-right: 0">
        <div class="square"></div>
      </div>
      <div class="col-md-7 two" style="padding:30px;">
        <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
        <p class="font" style="color:#199384;font-size:16px; font-weight:500">Россия <span style="margin-left:10px;">01.05.2018</span></p>
        <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

      </div>
      <div class="col-md-3 three" style="padding:30px;">
        <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
        <p class="color" style="color:#fff;">5000 Р</p>
        <button class="btn bigbtn call">Связаться</button>
      </div>

    </div>
    <div class="green_block row">
      <div class="col-md-2 one" style="padding:30px;padding-right: 0">
        <div class="square"></div>
      </div>
      <div class="col-md-7 two" style="padding:30px;">
        <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
        <p class="font" style="color:#199384;font-size:16px; font-weight:500">Россия <span style="margin-left:10px;">01.05.2018</span></p>
        <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

      </div>
      <div class="col-md-3 three" style="padding:30px;">
        <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
        <p class="color" style="color:#fff;">5000 Р</p>
        <button class="btn bigbtn call">Связаться</button>
      </div>

    </div>
    <div class="green_block row">
      <div class="col-md-2 one" style="padding:30px;padding-right: 0">
        <div class="square"></div>
      </div>
      <div class="col-md-7 two" style="padding:30px;">
        <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
        <p class="font" style="color:#199384;font-size:16px; font-weight:500">Россия <span style="margin-left:10px;">01.05.2018</span></p>
        <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

      </div>
      <div class="col-md-3 three" style="padding:30px;">
        <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
        <p class="color" style="color:#fff;">5000 Р</p>
        <button class="btn bigbtn call">Связаться</button>
      </div>

    </div>

  </div>
  <div class="col-md-12" style="text-align:center">
    <div class="pagination">
       <a href="#">&laquo;</a>
       <a href="#">1</a>
       <a class="active" href="#">2</a>
       <a href="#">3</a>
       <a href="#">4</a>
       <a href="#">5</a>
       <a href="#">6</a>
       <a href="#">&raquo;</a>
   </div>
</div>
</div>
<script>
switcher1.onclick=function(){

 if(/\*+/.test(phone_number.innerHTML))
 {
   phone_number.innerHTML="8-999-999-9999";
   this.innerHTML="Скрыть";
   return
 }
 phone_number.innerHTML="***********";
 this.innerHTML="Показать"
}
switcher2.onclick=function(){

 if(/\*+/.test(mail.innerHTML))
 {
   mail.innerHTML="googlecorporation@gmail.com";
   this.innerHTML="Скрыть";
   return
 }
 mail.innerHTML="**************************";
 this.innerHTML="Показать"
}
</script>
@endsection
