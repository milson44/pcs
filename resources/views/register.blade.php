@extends('layouts.app')

@section('content')

    <div class="container">


        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li class="nav-item active">
                <a class="nav-link bigfont blisspro" data-toggle="tab" href="#fiz" style="border-left: #fff;">Физическое лицо</a>
            </li>
            <li class="nav-item">
                <a class="nav-link bigfont blisspro" data-toggle="tab" href="#jur" style="border-right: #fff;">Юридическое лицо</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active " id="fiz">
                <div class="d-flex flex-column flex-lg-row">
                  <form class="form-horizontal" method="POST" action="">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="surname" class="col-md-12 control-label font" style="text-align:left;">Фамилия</label>
                                <input id="surname" type="text" class="form-control" name="name"  required autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="name" class="col-md-12 control-label font" style="text-align:left;">Имя</label>
                                <input id="name" type="text" class="form-control" name="kpp"  required autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="otchestvo" class="col-md-12 control-label font" style="text-align:left;">Отчество (если имеется)</label>
                                <input id="otchestvo" type="text" class="form-control" name="address"  autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="email" class="col-md-12 control-label font" style="text-align:left;">Эл. почта</label>
                                <input id="email" type="email" class="form-control" name="inn"  required autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="phone" class="col-md-12 control-label font" style="text-align:left;">Номер моб.телефона</label>
                                <input id="phone" type="text" class="form-control" name="phone" required autofocus >
                            </div>
                        </div>

                    </div>
                  <div class="col-md-6">

                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="passport" class="col-md-12 control-label font" style="text-align:left;">Серия и номер паспорта</label>
                            <input id="passport" type="text" class="form-control" name="ogrn"  required autofocus >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="address" class="col-md-12 control-label font" style="text-align:left;">Прописка</label>
                            <input id="address" type="text" class="form-control" name="ogrn"  required autofocus >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="inn" class="col-md-12 control-label font" style="text-align:left;">ИНН</label>
                            <input id="inn" type="text" class="form-control" name="ogrn"  required autofocus >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="password" class="col-md-12 control-label font" style="text-align:left;">Пароль</label>
                            <input id="password" type="text" class="form-control" name="ogrn"  required autofocus >
                        </div>
                    </div>

                  </div>
                </div>
                <div class="form-group" style="text-align:-webkit-center;">
                    <div class="reg">
                        <button type="submit" class="btn btn-primary" style="background-color:#086972; font-size:23px;">
                            Зарегистрироваться
                        </button>
                    </div>
                </div>
                </form>
                </div>
            </div>
            <div class="tab-pane fade" id="jur">
                <div class="d-flex flex-column flex-lg-row">
                  <form class="form-horizontal" method="POST" action="">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="name" class="col-md-12 control-label font" style="text-align:left;">Наименование</label>
                                <input id="name" type="text" class="form-control" name="name"  required autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="kpp" class="col-md-12 control-label font" style="text-align:left;">КПП</label>
                                <input id="kpp" type="text" class="form-control" name="kpp"  required autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 input_padd">
                              <label for="yuraddress" class="col-md-12 control-label font" style="text-align:left;">Юридический адрес</label>
                                <input id="yuraddress"  type="text" class="form-control" name="address"  required autofocus >
                            </div>
                        </div>

                    </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="inn" class="col-md-12 control-label font" style="text-align:left;">ИНН</label>
                            <input id="inn" type="text" class="form-control" name="inn" required autofocus >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="ogrn" class="col-md-12 control-label font" style="text-align:left;">ОГРН</label>
                            <input id="ogrn" type="text" class="form-control" name="ogrn"  required autofocus >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 input_padd">
                          <label for="phone" class="col-md-12 control-label font" style="text-align:left;">Номер моб.телефона</label>
                            <input id="phone" type="text" class="form-control" name="phone" required autofocus >
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group" style="text-align:-webkit-center;">
                    <div class="reg">
                        <button type="submit" class="btn btn-primary" style="background-color:#086972; font-size:23px;">
                            Зарегистрироваться
                        </button>
                    </div>
                </div>
                </form>

                </div>
            </div>
        </div>
</div>

@endsection
