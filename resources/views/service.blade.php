@extends('layouts.app')

@section('content')
<style>
.form-group{
  margin-bottom: 0px;
}
</style>
<div class="container-fluid">
   <ul class="nav navbar-nav navbar-left">
     <li class="blisspro" style="margin-right:40px;"><a class="color" href="#">Каталог услуг</a></li>
     <li class="blisspro"><a class="color" href="#">Помощь и поддержка</a></li>
   </ul>
   <ul class="nav navbar-nav navbar-right">
     <li class="blisspro"><a class="color" href="{{ url('/user') }}"><img src="{{asset('img/key.png')}}" style="width:25px; margin-right:10px; margin-top:-3px;"/>Личный кабинет</a></li>
   </ul>
   <div class="row">
     <div class=" col-md-12 ">
       <div class="search">
         <input type="search" name="q" placeholder="Поиск по сайту Введите название услуги"/>
         <i type="submit" class="fa fa-search fa-2x isearch" aria-hidden="true" style="margin-top:-3px"></i>
       </div>
     </div>
   </div>
 </div>
 <div class="container-fluid">
   <div class="row">
   <div class=" col-md-offset-4 col-md-8">
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

       <ul class="nav navbar-nav navbar-left font">
        <li style="float:left;"><a>Найдено 4</a></li>
      </ul>
       <ul class="nav navbar-nav navbar-right font">
       <li><a>Сортировать по</a></li>
       <li class="dropdown">
         <a href="#" class="dropdown-toggle color" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
           Цене <i class="fa fa-angle-down fa-lg"></i>
         </a>
         <ul class="dropdown-menu">
           <li><a href="#">Дате</a></li>
           <li><a href="#">Геопозиции</a></li>
         </ul>
       </li>
     </ul>
   </div>
 </div>
</div><style>
body{
    box-sizing: border-box;
}
</style>
   <div class="row">
     <div class="col-md-4">
       <div class="login color margin">
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check1" name="check"><label for="check1">Изобретения </label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check2" name="check"><label for="check2">Текст</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check3" name="check"><label for="check3">Аудиозапись</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check4" name="check"><label for="check4">Товарный знак</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check5" name="check"><label for="check5">Средства индивидуализации</label>
         </div>
         <div class="form-group checkbox1">
            <input class="check" type="checkbox" id="check6" name="check" checked><label for="check6">Видеозапись</label>
         </div>
       </div>
     </div>

   <div class=" col-md-8">
     <div class="" >

       <div class="green_block row">
         <div class="col-md-2 one" style="padding-right: 0;">
           <div class="square"></div>
         </div>
         <div class="col-md-7 two">
           <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
           <p class="font" style="color:#199384;font-size:16px; font-weight:600">Россия <span style="margin-left:10px;">01.05.2018</span></p>
           <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

         </div>
         <div class="col-md-3 three">
           <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
           <p class="color" style="color:#fff;">5000 Р</p>
           <button class="btn bigbtn call">Связаться</button>
         </div>

       </div>
       <div class="green_block row">
         <div class="col-md-2 one" style="padding-right: 0;">
           <div class="square"></div>
         </div>
         <div class="col-md-7 two">
           <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
           <p class="font" style="color:#199384;font-size:16px; font-weight:600">Россия <span style="margin-left:10px;">01.05.2018</span></p>
           <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

         </div>
         <div class="col-md-3 three">
           <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
           <p class="color" style="color:#fff;">5000 Р</p>
           <button class="btn bigbtn call">Связаться</button>
         </div>

       </div>
       <div class="green_block row">
         <div class="col-md-2 one" style="padding-right: 0;">
           <div class="square"></div>
         </div>
         <div class="col-md-7 two">
           <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
           <p class="font" style="color:#199384;font-size:16px; font-weight:600">Россия <span style="margin-left:10px;">01.05.2018</span></p>
           <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

         </div>
         <div class="col-md-3 three">
           <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
           <p class="color" style="color:#fff;">5000 Р</p>
           <button class="btn bigbtn call">Связаться</button>
         </div>

       </div>
       <div class="green_block row">
         <div class="col-md-2 one" style="padding-right: 0;">
           <div class="square"></div>
         </div>
         <div class="col-md-7 two">
           <p class="color" style="margin-bottom:0px; font-size:22px;">Наименование</p>
           <p class="font" style="color:#199384;font-size:16px; font-weight:600">Россия <span style="margin-left:10px;">01.05.2018</span></p>
           <p class="font blisspro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.</p>

         </div>
         <div class="col-md-3 three">
           <p class="color" style="color:#fff;font-size:25px;">Продаётся</p>
           <p class="color" style="color:#fff;">5000 Р</p>
           <button class="btn bigbtn call">Связаться</button>
         </div>

       </div>

     </div>
     <div class="col-md-12" style="text-align:center">
       <div class="pagination">
          <a href="#">&laquo;</a>
          <a href="#">1</a>
          <a class="active" href="#">2</a>
          <a href="#">3</a>
          <a href="#">4</a>
          <a href="#">5</a>
          <a href="#">6</a>
          <a href="#">&raquo;</a>
      </div>
   </div>
 </div>
  </div>
 </div>
@endsection
